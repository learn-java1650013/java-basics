package com.example.demo.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Employee;
import com.example.demo.Service.EmployeeService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class EmployeeController {
	@Autowired
	private EmployeeService employeeService;

	@PostMapping
	public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
		Employee createdEmployee = employeeService.createEmployee(employee);
		return new ResponseEntity<>(createdEmployee, HttpStatus.CREATED);
	}

	@GetMapping("/api/employees")
	public List<Employee> getAllEmployees() {
		return employeeService.getAllEmployees();
	}

	@DeleteMapping("api/v1/deleteById/{employeeid}")
	public void deleteEmployeeById(@PathVariable Integer employeeid) {
		employeeService.deleteEmployeeById(employeeid);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable Integer id) {
		Optional<Employee> employeeOptional = employeeService.getEmployeeById(id);

		if (employeeOptional.isPresent()) {
			// Employee exists, return it with HTTP 200 OK status
			return new ResponseEntity<>(employeeOptional.get(), HttpStatus.OK);
		} else {
			// Employee does not exist, return HTTP 404 Not Found
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	 @PutMapping("/{id}")
	    public ResponseEntity<Employee> updateEmployee(@PathVariable Long id, @RequestBody Employee employeeDetails) {
	        // Call the service to update the employee
	        Employee updatedEmployee = employeeService.updateEmployee(id, employeeDetails);

	        if (updatedEmployee != null) {
	            // Employee updated successfully
	            return new ResponseEntity<>(updatedEmployee, HttpStatus.OK);
	        } else {
	            // Employee not found
	            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	        }
	    }

}
