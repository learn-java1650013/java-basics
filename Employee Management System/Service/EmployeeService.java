package com.example.demo.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Employee;
import com.example.demo.Repository.EmployeeRepository;

@Service
public class EmployeeService {
	@Autowired
	private EmployeeRepository employeeRepository;

	public Employee createEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}

	public List<Employee> getAllEmployees() {
		return employeeRepository.findAll();
	}

	public void deleteEmployeeById(Integer employeeid) {
		employeeRepository.deleteById(employeeid);
	}

	public Optional<Employee> getEmployeeById(Integer id) {
		return employeeRepository.findById(id);
	}

	public Employee updateEmployee(Long id, Employee employeeDetails) {
		// Retrieve the employee by id
		Optional<Employee> employeeOptional = employeeRepository.findById(id);

		if (employeeOptional.isPresent()) {
			Employee existingEmployee = employeeOptional.get();

			// Update the details
			existingEmployee.setName(employeeDetails.getName());
			existingEmployee.setEmail(employeeDetails.getEmail());
			existingEmployee.setDepartment(employeeDetails.getDepartment());

			// Save and return the updated employee
			return employeeRepository.save(existingEmployee);
		}

		// Return null or throw an exception if the employee does not exist
		return null;
	}
}
