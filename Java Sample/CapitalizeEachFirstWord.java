package com.Java.Basics;

public class CapitalizeEachFirstWord {

	public static void main(String[] args) {
		String name = "this is awesome thing to do";
		
		String[] name1 = name.split(" ");
		StringBuilder sb = new StringBuilder();
		
		for(String string : name1) {
			if(name1.length > 1) {
			String s1 = string.substring(0,1).toUpperCase() + string.substring(1).toLowerCase();
			sb.append(s1 + " ");
			}
		}
		
		System.out.println(sb.toString().trim());
	}

}
