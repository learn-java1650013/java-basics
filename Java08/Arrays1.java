package leranJava08;

import java.util.Scanner;

public class Arrays1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int no1 = 1;
		System.out.println("Enter the Number : ");
		int no2 = sc.nextInt();
		Arrays1.printNos(no1,no2);
		
	}

	public static void printNos(int no1, int no2) {
		if(no1 < no2) {
			System.out.println(no1);
			no1++;
		}
		if (no1 < no2) {
			printNos(no1,no2);
		}
		
	}

}
