package leranJava08;

public class CountofDigits {

	public static void main(String[] args) {
		int no = 2345;
		
//		 To find the number of digits in no variable, we can use
//     	 the below two methods
//     	 1. String Concatenation
//     	 2. By using String valueOf and we can use the string length method
		
		String S = no + "";
		
		/*
		 * String T = String.valueOf(no); System.out.println(T.length());
		 */
		
		int c = countofDigits(no);
		double res = Math.pow(no, c);
		System.out.println(res);

	}

	public static int countofDigits(int no) {
		int count = 0;
		while(no > 0) {
			no = no /10;
			count++;
		}
		return count;
		
	}

}
