package leranJava08;

import java.util.Scanner;

public class ExceptionHandlingDemo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the First Number :");
		int no1 = sc.nextInt();

		System.out.println("Enter the Second Number :");
		int no2 = sc.nextInt();

		divide(no1, no2);
		add(no1, no2);
	}

	public static void add(int no1, int no2) {
		System.out.println(no1 + no2);

	}

	public static void divide(int no1, int no2) {
		try {
			System.out.println(no1 / no2);
			int[] arr = new int[no1];

			for (int i = 0; i < 10; i++) {
				System.out.println(arr[i]);
			}
		} catch (ArithmeticException ae) {
			//System.out.println("Arithmetic Error has occured");
			ae.printStackTrace(); 
			
		}
		catch (NegativeArraySizeException ne) {
			System.out.println("Check Array Length");
		} 
		catch (ArrayIndexOutOfBoundsException bound) {
			System.out.println("out of bound");
		}

	}

}
