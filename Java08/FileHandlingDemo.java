package leranJava08;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileHandlingDemo {

	public static void main(String[] args) throws IOException{
		File file = new File("/home/sree/Documents/EmployeeName.txt");
		file.createNewFile();
		
		FileWriter filewriter = new FileWriter(file);
		BufferedWriter bufferedwriter = new BufferedWriter(filewriter);
		bufferedwriter.write("Hi, It's First Day of Java");
		bufferedwriter.flush();
		bufferedwriter.close();
		
		FileReader filereader = new FileReader(file);
		if(filereader.read() != -1) {
			System.out.print(filereader.read());
		}
		filereader.close();
		
		

	}

}
