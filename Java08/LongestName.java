package leranJava08;

public class LongestName {

	public static void main(String[] args) {
		String[] names = {"apple","mango","orange","Banana"};
		int n = names.length;
        int[] arr = new int[n];
        
        for(int i =0; i < n;i++){
            arr[i] = names[i].length();
        }
		
        int longestName = 0;
        int result = 0;
        
      
        for(int i = 0; i < arr.length;i++){
        	 
        	 if(arr[i] > result) {
        		 result = i;	 
        	 }
        	 
         }
       
        System.out.println(names[result]); 
        

	}

}
