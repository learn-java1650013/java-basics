package leranJava08;
import java.util.Scanner;

public class OperatorsinJava {

	public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
       

//		  3.Comparison operators  --> <,>,<=,>=,==,!=
//		  4.Logical operators     --> &&,||,!
//		  5.Bitwise operators
		OperatorsinJava op1 = new OperatorsinJava();
		op1.arithmeticOperators();
		op1.assignmentOperators();

	}

	public void arithmeticOperators() {
		
//		 1.Arithmetic operators  --> +,-,*,/,%,++,--
		
		 int no1 = 10,no2 = 20;
		 System.out.println("n01 + no2 = " + (no1 + no2));
		 System.out.println("n01 - no2 = " + (no1 - no2));
		 System.out.println("n01 * no2 = " + (no1 * no2));
		 System.out.println("n01 / no2 = " + (no1 / no2));
		 System.out.println("n01 % no2 = " + (no1 % no2));
		
	}
	public void assignmentOperators() {
		
//		  2.Assignment operators  --> =,+=,-=,*=,/=,%=
		
		 int no1 = 10;  // to assign the values to a variable we've to use '=' operator
		 int no2 = 20;
		 no1 += 30;    //no1 = no1 + 30;
		 no2 -= 20;    //no2 = no2 - 20;
		 no1 *= 4;
		 no2 /= 5;
		 no1 %= 5;
		
	}

}
