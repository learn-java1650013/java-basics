package leranJava08;

public class Pattern {

	public static void main(String[] args) {
		for(int rows = 0; rows < 5; rows++) {
			for(int cols = 0; cols < 9; cols++) {
				if(cols == 4 || rows == 4 || cols == 4 + rows || cols == 4 - rows) {
					System.out.print(" * ");
				}
				else {
					System.out.print("   ");
				}
			}
			System.out.println();
		}

	}

}
