package leranJava08;

public class PowerOf2 {

	public static void main(String[] args) {
		int N = 1;
		boolean isPowerOfTwo = isPowerOfTwo(N);
		System.out.println(isPowerOfTwo);

	}

	public static boolean isPowerOfTwo(int n) {
		boolean powerof2 = false;
		double j = 2.0d;
		
		for(int i = 1; i <= n;i++) {
			if(n == Math.pow(j, i)){
				powerof2 = true;
				break;
			}
		}
		return (powerof2 == true)? true : false;
		
	}

}
