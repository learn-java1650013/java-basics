package leranJava08;

public class PrimitiveDatatypes {

	public static void main(String[] args) {
		
//		There are 8 primitive data types in Java. They are
//		byte -> short -> int -> long -> float -> double -> char -> boolean
		
//		To declare a variable, we have to define the data type first as shown below
//		int var_Name;
		
		byte    number1  = 10;      //(Stores whole numbers from -128 to 127)
		short   number2  = 100;     //(Stores whole numbers from -32,768 to 32,767)
		int     number3  = 5500;    //(Stores whole numbers from -2,147,483,648 to 2,147,483,647)
		long    number4  = 15000;   //(Stores whole numbers from -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807)
		float   number5  = 10.50f;  //(Stores fractional numbers. Sufficient for storing 6 to 7 decimal digits)
        double  number6  = 356.55d; //(Stores fractional numbers. Sufficient for storing 15 decimal digits)
        boolean number7  = false;   //(stores true or false values)
        char    number8  = '5';     //(Stores a single character/letter or ASCII values)
        
        System.out.println(number1);
        System.out.println(number2);
        System.out.println(number3);
        System.out.println(number4);
        System.out.println(number5);
        System.out.println(number6);
        System.out.println(number7);
        System.out.println(number8);
	}

}
