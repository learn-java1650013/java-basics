`package leranJava08;
import java.util.Scanner;

public class RecursionDemo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number : ");
		int no1 = 1;
		int no2 = sc.nextInt();
		printNumbers(no1,no2);

	}
	public static void printNumbers(int no1,int no2) {
		if(no1 <= no2)
		System.out.println(no1);
		no1++;
		if(no1 < no2) {
		printNumbers(no1,no2);
		}
	}

}
