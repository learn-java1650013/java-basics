package leranJava08;

public class TypeCastingDemo1 {

	public static void main(String[] args) {
		String Number = "127";
		String s11 = "32767";
		short no1 = 32767;
		
		int no2 = no1;
		System.out.println(no2);
		
//		// String to Primitive Data type, we have parse methods for each Data type
//		byte b1 = Byte.parseByte(Number);
//		System.out.println(b1);
//		
//		// String to Primitive Data type, we have parse methods for each Data type
//		short s1 = Short.parseShort(s11);
//		System.out.println(s1);
//		
//		// String to Primitive Data type, we have parse methods for each Data type
//		int i1 = Integer.parseInt(s11);
//		System.out.println(i1);
//		
//		// String to Primitive Data type, we have parse methods for each Data type
//		long l1 = Long.parseLong(s11);
//		System.out.println(l1);
//				
//		// String to Primitive Data type, we have parse methods for each Data type
//		float f1 = Float.parseFloat(s11);
//		System.out.println(f1);
//        
//		// String to Primitive Data type, we have parse methods for each Data type
//		double d1 = Double.parseDouble(s11);
//		System.out.println(d1);
//				
//		// String to Primitive Data type, we have parse methods for each Data type
//		boolean b11 = Boolean.parseBoolean(s11);
//		System.out.println(b11);
//	
//		String s12 = String.valueOf(no1);
//		System.out.println(s12);
	}
}
