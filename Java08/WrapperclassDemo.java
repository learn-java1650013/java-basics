package leranJava08;

public class WrapperclassDemo extends Sam{

	public static void main(String[] args) {
//		Primitive     Wrapper class
//		int           Integer
//		byte          Byte
//		short         Short
//		long          Long
//		float         Float
//		double        Double
//		char          Char
//		boolean       Boolean
		
		Sam s1 = new Sam();
		WrapperclassDemo wcd = new WrapperclassDemo();
		 s1.display(23);
		 
	}
	public void display(int ob) {
		System.out.println("object" + ob);
	}

}
